import React from 'react';
import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home(){
	// localStorage.setItem('url', '/home');

	const data = {
		title: "Bubba's Beverages",
		content: "Quench with Bubba's!",
		destination: "/products",
		label: "Shop now"
	}

	return(
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>		
	)
}