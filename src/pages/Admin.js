import { Row, Col, Card, Image, Button } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
export default function Admin(){
		
	return (

		<Row className="mt-4 mb-3">
		<Col xs={12} md={12} className="mt-1 mb-3">
		<h1 className="mt-1 mb-4">Admin Dashboard</h1>
		</Col>
			

			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light">
				  <Card.Body>
				    <Card.Title>
				    <h3>Products</h3>
				    </Card.Title>
				    <Card.Text>
				      View all active and inactive products
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				   	<Button variant="warning" type="submit" as={NavLink} to="/product-database">View products</Button> 	
				   	</div>
				  </Card.Body>
				</Card>
			</Col>


			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light">
				  <Card.Body>
				    <Card.Title>
				    <h3>Add Product</h3>
				    </Card.Title>
				    <Card.Text>
				      Add a new product
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				    <Button variant="warning" type="submit" as={NavLink} to="/add-product">Add product</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>


			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light">
				  <Card.Body>
				    <Card.Title>
				    <h3>User Database</h3>
				    </Card.Title>
				    <Card.Text>
				      View all customers and admins
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				    <Button variant="warning" type="submit" as={NavLink} to="/user-database">View users</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light mt-4">
				  <Card.Body>
				    <Card.Title>
				    <h3>Orders</h3>
				    </Card.Title>
				    <Card.Text>
				      View all orders
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				    <Button variant="warning" type="submit" as={NavLink} to="/order-database">View orders</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}