import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    function authenticate(e){
        e.preventDefault();
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

        if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access);

                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Welcome!",
                    icon: "success",
                    text: "Thirsty? Shop at Bubba's!"
                })
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
        })

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !==''))
        {
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])
    

    localStorage.setItem('id', user.id);
    localStorage.setItem('isAdmin', user.isAdmin);

        return(
            (user.id !== null) ?
            (user.isAdmin === true) ?

                <Redirect to="/admin" />
                :
                <Redirect to="/products" />
            :
            <Container className="auth-page">
                    <Row className="d-flex align-items-center">
                        <Col xs={12} lg={6} className="left-column">
                            <div className="widget-auth">
                                <div className="d-flex align-items-center justify-content-between py-3">
                                    <p className="auth-header mb-0">Sign In</p>
                                </div>

                            <Form onSubmit={(e) => authenticate(e)}>
                                <Form.Group controlId="userEmail">
                                    <Form.Text>Email address</Form.Text>
                                    <Form.Control 
                                        type="email" 
                                        placeholder="Enter email"
                                        className="input-transparent pl-3" 
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="password" className="mt-3">
                                    <Form.Text>Password</Form.Text>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Password" 
                                        className="input-transparent pl-3"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                    { isActive ? 
                                        <div className="bg-widget d-flex justify-content-center">
                                            <Button className="my-3" variant="outline-success" type="submit" id="submitBtn">
                                                <b>Submit</b>
                                            </Button>
                                        </div>
                                        : 
                                        <div className="bg-widget d-flex justify-content-center">
                                            <Button className="my-3" type="submit" id="submitBtn" variant="outline-dark" disabled>
                                                <b>Submit</b>
                                            </Button>
                                        </div>
                                    }
                            </Form>
                            </div>
                        </Col>
                    </Row>
                </Container>
                )
            }
