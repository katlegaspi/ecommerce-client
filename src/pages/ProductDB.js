import { Fragment, useEffect, useState } from 'react';
import AdminProductCard from '../components/AdminProductCard';
import { Container, Row } from 'react-bootstrap';


export default function Products(){
	const [products, setProducts] = useState([]);		


	useEffect(() => {
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			console.log(data);
		
			setProducts(data.map(product => {
					return (
						<AdminProductCard key={product.id} productProp={product} />
					)
				})
			);		
		})
	}, [])


	return (

		<Container>
			<Fragment> 
				<h1 className="mt-3 mb-4">Products</h1>
			</Fragment>
			<Row>
				<Fragment> 
					{products}
				</Fragment>
			</Row>
		</Container>
	)
}