import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Register(){
	//restructuring
    const {user, setUser} = useContext(UserContext);
    
    const history = useHistory();
    
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobile, setMobile] = useState('');

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	async function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
            	Swal.fire({
                	title: "Duplicate email found!",
                	icon: "error",
                	text: "Please provide a different email."
            	})
            }
            else {
   
					fetch('http://localhost:4000/users/register', {
			            method: 'POST',
			            headers: {
			                'Content-Type': 'application/json; charset=UTF-8'
			            },
			            body: JSON.stringify({

			                email: email,
			                password: password1,
			                firstName: firstName,
							lastName: lastName,
							mobileNo: mobile
			            })
			        })
			        .then(res => res.json())
			        .then(data => {
			            console.log(data);

			            if(data === true){

			                Swal.fire({
			                    title: "Registration successful",
			                    icon: "success",
			                    text: "Welcome to Zuitt!"
			                });  
			            }
			            else {
			                Swal.fire({
			                    title: "Authentication failed",
			                    icon: "error",
			                    text: "Check your login details and try again."
			                })
			            }
			        })

			        setFirstName('');
			        setLastName('');
					setEmail('');
					setMobile('');
					setPassword1('');
					setPassword2('');

					history.push("/login");
					
		}
	})				

}

     
		

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobile.length === 11) ){
			setIsActive(true);
		}
		else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobile, email, password1, password2] )

	return(
		// (user.id !== null) ?
  //           <Redirect to="/courses" />
  //       :
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
			  <Form.Label>First Name</Form.Label>
			  <Form.Control 
			  	type="firstName" 
			  	placeholder="Enter first name" 
			  	value={firstName}
			  	onChange = { e => setFirstName(e.target.value) }
			  	required />
			</Form.Group>

			<Form.Group controlId="lastName">
			  <Form.Label>Last Name</Form.Label>
			  <Form.Control 
			  	type="lastName" 
			  	placeholder="Enter last name" 
			  	value={lastName}
			  	onChange = { e => setLastName(e.target.value) }
			  	required />
			</Form.Group>

		  <Form.Group controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email" 
		    	value={email}
		    	onChange = { e => setEmail(e.target.value) }
		    	required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>   	
		  </Form.Group>

		  	<Form.Group controlId="mobile">
			  <Form.Label>Mobile Number</Form.Label>
			  <Form.Control 
			  	type="mobile" 
			  	placeholder="Enter mobile number" 
			  	value={mobile}
			  	onChange = { e => setMobile(e.target.value) }
			  	required />
			</Form.Group>

		  <Form.Group controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value={password1}
		    	onChange = { e => setPassword1(e.target.value) }
		    	required />
		  </Form.Group>
		  		
		  <Form.Group controlId="password2">
		  	<Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Verify Password" 
		    	value={password2}
		    	onChange = { e => setPassword2(e.target.value) }
		    	required />
		  </Form.Group>
		  		
		  	{ isActive ? 
		  		<Button variant="primary" type="submit" id="submitBtn">
		    		Submit
		  		</Button>
		  		: 
		  		<Button variant="danger" type="submit" id="submitBtn" disabled>
		    		Submit
		  		</Button>
			}
		</Form>
	)
}
