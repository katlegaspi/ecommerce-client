import React, { Fragment } from 'react';
import { useState, useEffect, useContext } from 'react';
import { Table, Container, Row, } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar';
import myOrderHistory from '../components/OrderHistory';


export default function OrderHistory(){

	const [order, setOrder] = useState([]);
	const [orderItems, setOrderItems] = useState([]);
	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`http://localhost:4000/users/myOrders`)
		.then(res => res.json())
		.then(data => {
			console.log(data);			
			
			setOrderItems(data[0].orderId.map(orderItem => {
				
				return(
					<OrderHistory key={orderItem._id} cartProp={orderItem} />
					)
				}
			))
			console.log(orderItems);
			
		})
	}, [])

	const checkOut = async () => {

	}

	return (
			
		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4">My Orders</h1>
		</Fragment>
		<Row>

		<Fragment> 
			{orderItems}
		</Fragment>
		</Row>
		</Container>
	)
}