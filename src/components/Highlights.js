import { Row, Col, Card, Image} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
			<Image src="https://images.unsplash.com/photo-1625865019554-220ea80ea813?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80" fluid />
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title><h2>Different drinks for different needs</h2></Card.Title>
				    <Card.Text>
				      Water, softdrinks, juice and more varieties to choose from.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
			<Image src="https://cdn.pixabay.com/photo/2017/08/03/21/48/drinks-2578446_960_720.jpg" fluid />
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title><h2>Drinks for all occasions</h2></Card.Title>
				    <Card.Text>
				      You'll never be boring at any event if you bring these drinks along!
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
			<Image src="https://static5.depositphotos.com/1031343/421/v/600/depositphotos_4214680-stock-illustration-limited-edition-stamp.jpg" fluid />
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title><h2>Beverage of the month</h2></Card.Title>
				    <Card.Text>
				      Check out our team's limited edition beverage or suggest one!
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}