import { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){
	const {user} = useContext(UserContext);
	
	return(

		(user.isAdmin === true) ?
			<Navbar bg="warning" variant="light">
  			<Navbar.Brand as={NavLink} to="/" exact>Bubba's</Navbar.Brand>
     		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		 			<Navbar.Collapse id="basic-navbar-nav">
    				<Nav className="ml-auto">
    					<Nav.Link as={NavLink} to="/admin" exact>Dashboard</Nav.Link>
    					<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
    				</Nav>
		    	</Navbar.Collapse>
			</Navbar>:		    				


			<Navbar bg="warning" variant="light">

	    	<Navbar.Brand as={NavLink} to="/" exact>Bubba's</Navbar.Brand>
	     		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		 			<Navbar.Collapse id="basic-navbar-nav">
    				<Nav className="ml-auto">
    					{(user.id !== null) ?
    					  <Fragment>
    					  <Nav.Link as={NavLink} to="/cart" exact>My Cart</Nav.Link>
    					  <Nav.Link as={NavLink} to="/order-history" exact>My Order History</Nav.Link>
    					  <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
    					  </Fragment>
    					  :
    					  <Fragment>
    					  	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
    							<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
    					  </Fragment>
    					}
    				</Nav>
  			</Navbar.Collapse>
	  	</Navbar>
	);
}
