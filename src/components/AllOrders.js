import { Table } from 'react-bootstrap';

export default function AllOrderTable({orderProp}) {
	const {_id, purchasedOn, productName, totalAmount} = orderProp;
	
		return (
			<div>
					<Table striped bordered hover className="mt-2">
					  
					  <tbody>
					    <tr>
					      <td>{_id}</td>
					      <td>{purchasedOn}</td>
					      <td>{productName}</td>
					      <td>{totalAmount}</td>
					    </tr>
					  </tbody>
					</Table>
			</div>
			)
};
